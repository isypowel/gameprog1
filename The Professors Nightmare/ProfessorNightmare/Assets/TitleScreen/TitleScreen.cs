using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when play is pressed
    public void OnClick()
    {
        //Debug.Log("in OnClick");
        SceneManager.LoadScene("MainScene");
    }

    //when about is pressed
    public void OnClick2()
    {
        //Debug.Log("in OnClick2");
        SceneManager.LoadScene("About");
    }
}
