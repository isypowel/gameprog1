using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when restart is pressed
    public void OnClick()
    {
        //Debug.Log("in OnClick");
        SceneManager.LoadScene("MainScene");
    }

    //when quit is pressed
    public void OnClick2()
    {
        //Debug.Log("in OnClick2");
        Application.Quit();

    }
}
