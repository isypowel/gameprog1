using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClareScript : MonoBehaviour
{
    //public GameObject Clare;
    //GameObject ClareInstance;
    public Animator animator;
    GameManScript gm;
    GameObject obj;
    Rigidbody2D rb;
    public Transform player;
    Vector3 direction;
    Vector2 movement;
    public bool chase;
    bool leave;
    Vector2 exit;
    public AudioSource[] sounds;
    public AudioSource walkingSound;
    public AudioSource appeardSound;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("clare is awake");
        animator = GetComponent<Animator>();
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManScript>();
        rb = this.GetComponent<Rigidbody2D>();

        gm.clarePresent = true;
        chase = false;
        leave = false;

        //sound stuff
        sounds = GetComponents<AudioSource>();
        walkingSound = sounds[0];
        appeardSound = sounds[1];

        appeardSound.Play();
    }

    // Update is called once per frame
    void Update()
    {
        direction = player.position - transform.position;
        direction.Normalize();
        movement = direction;
    }

    private void FixedUpdate()
    {
        if(chase) {
            rb.MovePosition((Vector2)transform.position + (movement * Time.deltaTime));
        }
        if(leave)
        {
            rb.MovePosition((Vector2)transform.position + (exit * Time.deltaTime));
        }

    }
    
    public void findHer()
    {
        animator.SetBool("CWaling", true);
        chase = true;
        gm.paperGraded = false;
    }

    public void confused()
    {
        animator.SetBool("CWaling", false);
        chase = false;
        animator.SetBool("CConfused", true);
    }

    public void cExit()
    {
        animator.SetBool("CConfused", false);
        animator.SetBool("CWaling", true);
        leave = true;
        Destroy(gm.ClareInstance, 2);
        gm.clarePresent = false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Player")
        {
            chase = false;
            animator.SetBool("CWaling", false);
            gm.playerCaught();
        }
    }
}
