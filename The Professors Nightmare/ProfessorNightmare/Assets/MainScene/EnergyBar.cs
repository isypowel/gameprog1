using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBar : MonoBehaviour
{
    public Transform bar;
    float maxEnergy = 1f;
    public bool introOver = false;
    // Start is called before the first frame update
    void Start()
    {
        bar = transform.Find("Bar");
        bar.localScale = new Vector3(1f, 1f);

    }

    // Update is called once per frame
    void Update()
    {
        if(introOver)
        {
            bar.localScale = new Vector3(bar.localScale.x - (Time.deltaTime / 100), 1f);
        }
    }

    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);
    }

    public void AddEnergy()
    {
        if(bar.localScale.x + .2f >= maxEnergy)
        {
            bar.localScale = new Vector3(1f, 1f);
        } else
        {
            bar.localScale = new Vector3(bar.localScale.x + 0.2f, 1f);
        }
    }
}
