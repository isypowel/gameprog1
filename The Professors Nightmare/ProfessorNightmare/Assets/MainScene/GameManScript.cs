using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManScript : MonoBehaviour
{
    public bool isHiding;
    public bool isInter;
    public bool isMoving;
    public bool paperGraded;
    public PlayerScript player;
    public GameObject clare;
    public GameObject ClareInstance;
    public ClareScript clareScript;
    public Transform playerTrans;
    GameObject obj;
    GameObject obj2;
    public bool clarePresent;
    public Text youFound;
    public Text paperCount;
    public new Camera camera;
    public int papers;
    public Coroutine introCoroutine;
    public GameObject BlackFade;
    GameObject BlackFadeInst;
    public EnergyBar energyBarScript;
    GameObject obj3;
    public AudioSource[] sounds;
    public AudioSource itemFoundSound;
    public AudioSource nothingHereSound;

    // Start is called before the first frame update
    void Start()
    {
        obj = GameObject.FindGameObjectWithTag("Player");
        player = obj.GetComponent<PlayerScript>();
        papers = 0;

        introCoroutine = StartCoroutine(Intro());

        obj3 = GameObject.FindGameObjectWithTag("Energy");
        energyBarScript = obj3.GetComponent<EnergyBar>();

        //sound stuff
        sounds = GetComponents<AudioSource>();
        itemFoundSound = sounds[0];
        nothingHereSound = sounds[1];
    }

    // Update is called once per frame
    void Update()
    {
        //if all papers are found
        if (papers == 5)
        {
            SceneManager.LoadScene("YouWin");
        }
        
        //if the energy bar completely depletes
        if (energyBarScript.bar.localScale.x <= 0f)
        {
            if (ClareInstance == null)
            {
                SceneManager.LoadScene("GameOver");
            } else
            {
                Destroy(ClareInstance);
                SceneManager.LoadScene("GameOver");
            }
        }
    }

    //will run an intro scene for the game
    protected IEnumerator Intro() {
        player.pauseInput = true;

        //a cool fade-in
        BlackFadeInst = Instantiate(BlackFade);
        yield return new WaitForSeconds(1);
        BlackFadeInst.GetComponent<Animator>().SetBool("IsFade", true);
        yield return new WaitForSeconds(4);
        Destroy(BlackFadeInst);

        //player appears in bedroom
        player.transform.position = new Vector3(17.8f, -4.19f, 0);

        camera.enabled = true;
        camera.orthographic = true;
        camera.orthographicSize = 4.95f;
        camera.transform.position = new Vector3(18.63f, -.17f, -10);

        //some dialogue
        youFound.text = @"Oh! I forgot to grade those papers!";
        Invoke("clearText", 5);

        yield return new WaitForSeconds(5);

        ClareInstance = GameObject.Instantiate<GameObject>(clare, new Vector3(14.76f, -.94f, 0), Quaternion.identity);
        obj2 = GameObject.FindGameObjectWithTag("Clare");
        clareScript = obj2.GetComponent<ClareScript>();
        clareScript.player = playerTrans;

        youFound.text = @"oop...";
        Invoke("clearText", 1);
        yield return new WaitForSeconds(1);

        player.animator.SetBool("IsHiding", true);
        player.renderer.color = new Color(1, 1, 1, 0);

        youFound.text = @"WHY IS MY GRADE BAD!?!?!";
        Invoke("clearText", 3);
        yield return new WaitForSeconds(3);
        hide();

        yield return new WaitForSeconds(8);

        player.animator.SetBool("IsHiding", false);
        player.renderer.color = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(2);

        youFound.text = @"Clare has always been a proactive student...";
        Invoke("clearText", 3);
        yield return new WaitForSeconds(3);

        youFound.text = @"I'm pretty sleepy...I know I have my energy pills around here somewhere...and the rest of those papers...";
        Invoke("clearText", 6);
        yield return new WaitForSeconds(6);


        player.pauseInput = false;
        
        energyBarScript.introOver = true;
        yield return null;
    }

    //when the player interacts with an object
    public void interact(GameObject interSpot, string item)
    {
        if (item == "paper")
        {
            player.pauseInput = true;
            youFound.text = "You found a Paper";
            itemFoundSound.Play();
            papers++;
            paperCount.text = papers + "/5";
            player.animator.SetBool("IsGrading", true);
            paperGraded = true;

        } else if (item == "energy")
        {
            youFound.text = "You found some Energy!";
            itemFoundSound.Play();
            energyBarScript.AddEnergy();

        } else if (item == "both")
        {
            player.pauseInput = true;
            youFound.text = "You found a Paper and some Energy!";
            itemFoundSound.Play();
            papers++;
            paperCount.text = papers + "/5";
            player.animator.SetBool("IsGrading", true);
            paperGraded = true;
            energyBarScript.AddEnergy();

        } else if (item == "neither" && !(youFound.text == "You found a Paper") &&
                   !(youFound.text == "You found some Energy!") && !(youFound.text == "You found a Paper and some Energy!"))
        {
            youFound.text = "Nothing here";
            nothingHereSound.Play();
        }

        Invoke("clearText", 5);
    }

    public void hide()
    {
        player.hide = true;
        if (clarePresent)
        {
            clareScript.confused();
        }
    }

    public void door(string doorSpot)
    {
        if (doorSpot == "Kit2LivDoor")
        {
            //now in the living room
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 4.95f;
            camera.transform.position = new Vector3(0.87f, -.17f, -10);
            player.transform.position = new Vector3(-6.8f, player.transform.position.y, 0);

        } else if (doorSpot == "Liv2KitDoor")
        {
            //now in kitchen
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 4.95f;
            camera.transform.position = new Vector3(-16.88f, -.4f, -10);
            player.transform.position = new Vector3(-9.42f, player.transform.position.y, 0);

        } else if (doorSpot == "Liv2BedDoor")
        {
            //now in bedroom
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 4.95f;
            camera.transform.position = new Vector3(18.63f, -.17f, -10);
            player.transform.position = new Vector3(14.55f, player.transform.position.y, 0);

        } else if (doorSpot == "Bed2LivDoor")
        {
            //now in living room
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 4.95f;
            camera.transform.position = new Vector3(0.87f, -.17f, -10);
            player.transform.position = new Vector3(5.01f, player.transform.position.y, 0);

        } else if (doorSpot == "Bed2BathDoor")
        {
            //now in bathroom
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 3.3f;
            camera.transform.position = new Vector3(18.63f, 9.99f, -10);
            player.transform.position = new Vector3(player.transform.position.x, 7.15f, 0);

        } else if (doorSpot == "Bath2BedDoor")
        {
            //now in bedroom
            camera.enabled = true;
            camera.orthographic = true;
            camera.orthographicSize = 4.95f;
            camera.transform.position = new Vector3(18.63f, -.17f, -10);
            player.transform.position = new Vector3(player.transform.position.x, -.78f, 0);
        }
    }

    //moving Clare to the room she triggered
    public void clareDoor(string doorSpot)
    {
        Debug.Log("in gm.clareDoor");
        if (doorSpot == "Kit2LivDoor")
        {
            //clare is now in living room
            ClareInstance.transform.position = new Vector3(-6.47f, ClareInstance.transform.position.y, 0);

        } else if (doorSpot == "Liv2KitDoor")
        {
            //clare is now in kitchen
            ClareInstance.transform.position = new Vector3(-9.5f, ClareInstance.transform.position.y, 0);

        } else if (doorSpot == "Liv2BedDoor")
        {
            //clare is now in bedroom
            ClareInstance.transform.position = new Vector3(14.73f, ClareInstance.transform.position.y, 0);

        } else if (doorSpot == "Bed2LivDoor")
        {
            //clare is now in living room
            ClareInstance.transform.position = new Vector3(4.8f, ClareInstance.transform.position.y, 0);

        } else if (doorSpot == "Bed2BathDoor")
        {
            //clare is now in bathroom
            ClareInstance.transform.position = new Vector3(ClareInstance.transform.position.x, 7.12f, 0);

        } else if (doorSpot == "Bath2BedDoor")
        {
            //clare is now in bedroom
            ClareInstance.transform.position = new Vector3(ClareInstance.transform.position.x, -.64f, 0);
        }
    }

    public void releaseTheBeast()
    {
        player.pauseInput = false;
        if(ClareInstance == null)
        {
            ClareInstance = GameObject.Instantiate<GameObject>(clare, new Vector3(-2.15f, -.61f, 0), Quaternion.identity);
            obj2 = GameObject.FindGameObjectWithTag("Clare");
            clareScript = obj2.GetComponent<ClareScript>();
            clareScript.player = playerTrans;
        }
        player.animator.SetBool("IsGrading", false);
        paperGraded = true;

        Invoke("release2", 2.0f);
    }

    public void release2()
    {
        clareScript.findHer();
    }

    public void playerCaught() {
        player.pauseInput = true;
        clareScript.animator.SetBool("CAttack", true);
        player.animator.SetBool("IsHurt", true);
    }

    public void playerHurt()
    {
        Destroy(ClareInstance, 3);
        if(ClareInstance == null)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    public void clearText()
    {
        youFound.text = "";
    }
}
