using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactables : MonoBehaviour
{
    List<GameObject> InteractablesList;
    public List<GameObject> ContainsPaper;
    public List<GameObject> ContainsEnergy;
    GameManScript gm;
    GameObject obj;
    public Triggers t;
    GameObject obj2;

    // Start is called before the first frame update
    void Start()
    {
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManScript>();
        obj2 = GameObject.FindGameObjectWithTag("IntrManager");
        t = obj.GetComponent<Triggers>();

        InteractablesList = new List<GameObject>();
        ContainsEnergy = new List<GameObject>();
        ContainsPaper = new List<GameObject>();

        //adding the interactables to the list
        GameObject originalGameObject = GameObject.Find("Interactables");
        //17 is the number of children Interactables has in totlal
        for (int i=0; i<16; i++)
        {
            GameObject child = originalGameObject.transform.GetChild(i).gameObject;
            if(child.tag == "Interaction" || child.tag == "HideAndInter")
            {
                InteractablesList.Add(child);
            }
        }

        //adding 5 paper to one of the interactables randomly
        InteractablesList = permutation(InteractablesList);
        for(int e =0; e<5; e++)
        {
            ContainsPaper.Add(InteractablesList[e]);
        }

        //randomly adding enerygy componenets to the Interactables
        InteractablesList = permutation(InteractablesList);
        for (int e = 0; e < 9; e++)
        {
            ContainsEnergy.Add(InteractablesList[e]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //permutation
    public List<GameObject> permutation(List<GameObject> ar)
    {
        var rand = new System.Random();
        for (int i= 0;i<ar.Count; i++) {
            int r = rand.Next(0, ar.Count-1);
            GameObject swap = ar[r];
            ar[r] = ar[i];
            ar[i] = swap;
        }

        return ar;
    }
}
