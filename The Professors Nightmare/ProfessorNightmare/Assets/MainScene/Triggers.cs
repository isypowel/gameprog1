using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggers : MonoBehaviour
{
    GameManScript gm;
    GameObject obj;
    public bool hasPaper;
    public bool hasEnergy;
    public List<GameObject> InterList;
    Interactables interact;
    GameObject obj2;

    // Start is called before the first frame update
    void Start()
    {
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManScript>();
        obj2 = GameObject.FindGameObjectWithTag("IntrManager");
        interact = obj2.GetComponent<Interactables>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log(other.gameObject.name + " triggered me, " + this.gameObject.name);

        //when the player triggers
        if (other.gameObject.name == "Player")
        {
            //if the player is trying to hide
            if ((this.gameObject.tag == "HidingSpot" || this.gameObject.tag == "HideAndInter")
                && gm.isHiding)
            {
                gm.isHiding = true;
                gm.hide();
            } else
            {
                gm.isHiding = false;
            }

            //if the player is trying to interact
            if((this.gameObject.tag == "Interaction" || this.gameObject.tag == "HideAndInter")
                && gm.isInter)
            {
                //if this object has energy
                if (interact.ContainsEnergy.Contains(this.gameObject)
                    && !interact.ContainsPaper.Contains(this.gameObject))
                {
                    gm.interact(this.gameObject, "energy");
                    interact.ContainsEnergy.Remove(this.gameObject);
                } else if (interact.ContainsPaper.Contains(this.gameObject)
                    && !interact.ContainsEnergy.Contains(this.gameObject))
                {
                    //if this object has a paper
                    gm.interact(this.gameObject, "paper");
                    interact.ContainsPaper.Remove(this.gameObject);
                } else if (interact.ContainsPaper.Contains(this.gameObject)
                    && interact.ContainsEnergy.Contains(this.gameObject))
                {
                    //if this object has both
                    gm.interact(this.gameObject, "both");
                    interact.ContainsPaper.Remove(this.gameObject);
                    interact.ContainsEnergy.Remove(this.gameObject);
                } else if(!interact.ContainsPaper.Contains(this.gameObject)
                    && !interact.ContainsEnergy.Contains(this.gameObject))
                {
                    //if this object has neither
                    gm.interact(this.gameObject, "neither");
                }
                
            } else
            {
                gm.isInter = false;
            }

            //if the player is trying to change rooms
            if(this.gameObject.tag == "Door" && gm.isMoving)
            {
                gm.door(this.gameObject.name);
            } else
            {
                gm.isMoving = false;
            }
        }

        //if clare triggers a door
        if(other.gameObject.name == "Clare(Clone)" && this.gameObject.tag == "Door")
        {
            gm.clareDoor(this.gameObject.name);
        }
    }
}
