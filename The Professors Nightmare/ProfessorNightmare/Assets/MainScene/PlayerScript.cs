using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Animator animator;
    public new SpriteRenderer renderer;
    GameManScript gm;
    GameObject obj;
    public bool hide;
    public bool pauseInput;
    public AudioSource[] sounds;
    public AudioSource walkingSound;
    public AudioSource gradeSound;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManScript>();
        pauseInput = false;

        //sound stuff
        sounds = GetComponents<AudioSource>();
        walkingSound = sounds[0];
        gradeSound = sounds[1];
    }

    // Update is called once per frame
    void Update()
    {
        if(!pauseInput)
        {
            //walking
            if (Input.GetButton("Horizontal") == true || Input.GetButton("Vertical") == true)
            {
                animator.SetBool("IsWalking", true);
                //walkingSound.Play();
                gm.isMoving = true;
            }
            else
            {
                animator.SetBool("IsWalking", false);
                gm.isMoving = false;
            }

            //walking U and D
            bool WSpressed = Input.GetButton("Vertical");
            float axis2 = WSpressed ? 1 : 0;
            if (Input.GetAxis("Vertical") < -.01)
                axis2 = -axis2;

            transform.Translate(new Vector3(0f, axis2 * Time.deltaTime, 0f));

            //walking L and R
            bool ADpressed = Input.GetButton("Horizontal");
            float axis1 = ADpressed ? 1 : 0;
            if (Input.GetAxis("Horizontal") < -.01)
                axis1 = -axis1;

            transform.Translate(new Vector3(axis1 * Time.deltaTime, 0f, 0f));

            //hiding (space bar) (letting other scripts know)
            if (Input.GetButton("Hide"))
            {
                gm.isHiding = true;
            }
            else
            {
                gm.isHiding = false;
                animator.SetBool("IsHiding", false);
                hide = false;
            }

            //actual hiding
            if (hide)
            {
                animator.SetBool("IsHiding", true);
                renderer.color = new Color(1, 1, 1, 0);
            }
            else
            {
                animator.SetBool("IsHiding", false);
                renderer.color = new Color(1, 1, 1, 1);
            }

            //interacting (m)
            if (Input.GetButton("Interact"))
            {
                gm.isInter = true;
            }
            else
            {
                gm.isInter = false;
            }
        }
    }

    public void doneGrading()
    {
        gradeSound.Play();
        gm.releaseTheBeast();
    }

    public void hurt()
    {
        gm.playerHurt();
    }

    public void canMove()
    {
        pauseInput = false;
    }
}
