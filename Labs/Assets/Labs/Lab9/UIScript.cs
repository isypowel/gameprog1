using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    public int count;
    public GameObject circle;
    public GameObject star;
    public GameObject square;
    public bool isCircle;
    public bool isStar;
    public bool isSquare;
    List<GameObject> shapes = new List<GameObject>();

    //when "Release is pressed, instantiate the number
    //of the shape requested, and add each of them to the shapes list
    public void OnClick()
    {
        //Debug.Log("release");
        //Debug.Log("count is " + count);

        //if the user didn't put a number in the input field
        if(count == 0)
        {
            MonoBehaviour.print("Please put a value in the field!");
        }

        if (isCircle==true)
        {
            for(int i=0; i<count; i++)
            {
                GameObject obj = Instantiate(circle, new Vector3(Random.Range(-4.5f, 6f), 10, 0),
                                             Quaternion.identity);
                shapes.Add(obj);
                
            }

        } else if(isStar==true)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject obj = Instantiate(star, new Vector3(Random.Range(-4.5f, 6f), 10, 0),
                                             Quaternion.identity);
                shapes.Add(obj);
            }

        } else if(isSquare==true)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject obj = Instantiate(square, new Vector3(Random.Range(-4.5f, 6f), 10, 0),
                                             Quaternion.identity);
                shapes.Add(obj);
            }
        }
    }

    //when "Clear" is pressed, it will remove all of the shapes
    //and clear the shape list
    public void OnClick2()
    {
        //Debug.Log("restart");
        shapes.ForEach(Destroy);
        shapes.Clear();

    }

    //when the value in "How many?" is done being edited
    public void ReadStringInput(string i)
    {
        count = int.Parse(i);
        //Debug.Log(count);
    }

    //when circle is toggled
    public void isCircleShape(bool yes)
    {
        isCircle = yes;
        //Debug.Log("circle" + isCircle);
    }

    //when star is toggled
    public void isStarShape(bool yes)
    {
        isStar = yes;
        //Debug.Log("star" + isStar);
    }

    //when square is toggled
    public void isSquareShape(bool yes)
    {
        isSquare = yes;
        //Debug.Log("square" + isSquare);
    }
}
