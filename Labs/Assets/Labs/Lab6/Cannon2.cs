using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon2 : MonoBehaviour
{
    public GameObject ball;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        if (ball == null)
            Debug.LogError("You forgot to set the prefab");
    }

    // Update is called once per frame
    void Update()
    {
        InvokeRepeating("SpawnCannonBall2", 0.001f, 6f);
        animator.SetBool("CannonBall2", true);
    }

    public void SpawnCannonBall2(Object nullobj)
    {
        GameObject obj = GameObject.Instantiate<GameObject>(ball);

        Destroy(obj, 2);
    }

    public void SwitchTransition2(Object nullobj)
    {
        animator.SetBool("CannonBall2", false);
    }
}
