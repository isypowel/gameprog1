using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonScript : MonoBehaviour
{
    public GameObject ball;

    // Start is called before the first frame update
    void Start()
    {

        if (ball == null)
            Debug.LogError("You forgot to set the prefab");
    }

    // Update is called once per frame
    void Update()
    {
        Animator animator = GetComponent<Animator>();

        //set if they pressed the jump button
        if (Input.GetButtonDown("Jumping"))
        {
            animator.SetBool("CannonBall", true);
        }
    }

    public void SpawnCannonBall(Object nullobj)
    {
        GameObject obj = GameObject.Instantiate<GameObject>(ball);

        Destroy(obj, 2);
    }

    public void SwitchTransition(Object nullobj)
    {
        Animator animator = GetComponent<Animator>();
        animator.SetBool("CannonBall", false);
    }
}
