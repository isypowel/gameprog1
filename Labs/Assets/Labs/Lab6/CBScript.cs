using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBScript : MonoBehaviour
{
    public float speed;
    public bool isRightFacing;
    public bool firstball; //is this the ball from the first or second cannon?
    // Start is called before the first frame update
    void Start()
    {
        if(speed ==0)
            Debug.LogError("You forgot to set the speed to non-zero");
    }

    // Update is called once per frame
    void Update()
    {
        if(firstball) //first ball
        {
            if (isRightFacing)
            {
                transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
            }
            else
            {
                SpriteRenderer sprite = GetComponent<SpriteRenderer>();
                if (sprite.flipX == false)
                    sprite.flipX = true;

                transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
            }
        } else //second ball
        {
            if (isRightFacing)
            {
                transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
            }
            else
            {
                SpriteRenderer sprite = GetComponent<SpriteRenderer>();
                if (sprite.flipX == false)
                    sprite.flipX = true;

                transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
            }
        }
    }
}
