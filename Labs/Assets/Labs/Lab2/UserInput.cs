using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour
{
    public float speed;
    public Animator animator;
    public SpriteRenderer renderer;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        bool pressed = Input.GetButton("Horizontal");
        float axis = pressed ? 1 : 0;
        if (renderer.flipX == true)
            axis = -axis;

        transform.Translate(new Vector3(axis * speed * Time.deltaTime, 0f, 0f));

        if (animator)
        {
            //running
            if (Input.GetButton("Horizontal") == true)
                animator.SetBool("IsRunning", true);
            else
                animator.SetBool("IsRunning", false);

            //falling
            if (Input.GetButton("Falling") == true)
            {
                animator.SetBool("IsFalling", true);
                transform.Translate(0f, 1f, 0f);
            }
            else
            {
                animator.SetBool("IsFalling", false);
                transform.position = new Vector3(transform.position.x, -1, 0);
            }

            //jumping
            if (Input.GetButton("Jumping") == true)
            {
                animator.SetBool("IsJumping", true);
                transform.position = new Vector3(transform.position.x, 1, 0);
            }
            else
            {
                animator.SetBool("IsJumping", false);
                transform.position = new Vector3(transform.position.x, 0, 0);
            }
                
            //flipping
            if (Input.GetAxis("Horizontal") > .01)
                renderer.flipX = false;
            else if (Input.GetAxis("Horizontal") < -.01)
                renderer.flipX = true;
        }
    }
}
