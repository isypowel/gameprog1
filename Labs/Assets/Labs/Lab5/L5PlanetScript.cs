using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L5PlanetScript : MonoBehaviour
{
    MCScript mwp;

    public Bounds bounds;
    public Color pressColor;
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        mwp = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MCScript>();
        if(mwp == null)
        {
            Debug.LogError("Unable to find mouse world position component");
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, rotationSpeed * Time.deltaTime), Space.Self);

        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        bounds = renderer.bounds;

        Vector3 wp = new Vector3(mwp.mouseWorldPos.x, mwp.mouseWorldPos.y, transform.position.z);
        
        if (renderer.bounds.Contains(wp) && Input.GetButton("MouseClick"))
        {
            renderer.color = pressColor;
        }
        else
        {
            renderer.color = Color.white;
        }
    }
}
