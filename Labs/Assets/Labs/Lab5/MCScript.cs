using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCScript : MonoBehaviour
{
    public Vector3 mouseWorldPos;
    public Vector3 worldPnt;
    public float movementSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //grab camera
        Camera camera = GetComponent<Camera>();

        //use the camera's transform to convert a screen point to a world point
        worldPnt = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
                                                         Input.mousePosition.y,
                                                         Input.mousePosition.z));
        mouseWorldPos = new Vector3(worldPnt.x, worldPnt.y, 0);

        //moving the main camera
        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");

        if(Camera.current != null)
        {
            Camera.main.transform.Translate(new Vector3((-xAxis * movementSpeed * Time.deltaTime),
                                                        (-yAxis * movementSpeed  * Time.deltaTime), 0));
        }
    }
}