using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L5Star : MonoBehaviour
{
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 parentPos = transform.parent.transform.position; //getting the parent's position

        transform.RotateAround(parentPos, Vector3.forward, rotationSpeed * Time.deltaTime);
    }
}
