using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicPlanet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        transform.Translate(new Vector3(5 * Time.deltaTime, 5 * Time.deltaTime, 0));

        if(transform.position.x >= 10)
        {
            transform.position = new Vector3(-5, -5, 0);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name + " triggered me!");
    }
}
