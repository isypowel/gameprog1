using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject House;
    public GameObject obj;
    public GameObject Missle;
    public GameObject obj2;
    // Start is called before the first frame update
    void Start()
    {
        obj = Instantiate(House);
        obj2 = Instantiate(Missle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DestroyHouse()
    {
        Destroy(obj);
        Debug.Log("after destroy house");
    }

    public void DestroyMissle()
    {
        Destroy(obj2);
        Debug.Log("after destroy missle");
    }
}
