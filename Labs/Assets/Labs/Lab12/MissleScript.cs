using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleScript : MonoBehaviour
{
    public GameObject obj;
    public GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("missle alive");
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "House")
        {
            Debug.Log("missle collision");
            gm.DestroyMissle();
        }
    }
}
