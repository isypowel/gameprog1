using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMessageTimer : MonoBehaviour
{
    public float duration;
    private float endTime;
    private bool timerStarted;
    public GameObject calledObject;

    // Start is called before the first frame update
    void Start()
    {
        timerStarted = false;
        StartTimer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float TimeLeft()
    {
        if(timerStarted == false)
        {
            return 0;
        }else
        {
            return endTime - Time.time;
        }
    }

    public void StartTimer()
    {
        timerStarted = true;
        StartCoroutine(StartCountdown());
    }

    protected virtual IEnumerator StartCountdown()
    {
        //calcul end time from current time and duration
        endTime = Time.time + duration;

        //loop until the elapsed time is greater than end time
        while(Time.time < endTime)
        {
            calledObject.SendMessage("OnTimerTick", this);

            yield return null; //returns control back to game
        }

        calledObject.SendMessage("OnTimerExpired");
    }
}
