using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUpdate : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTimerTick(SendMessageTimer timer)
    {
        Text text = gameObject.GetComponentInChildren<Text>();
        text.text = "" + timer.TimeLeft();
    }

    public void OnTimerExpired()
    {
        Text text = gameObject.GetComponentInChildren<Text>();
        text.text = "0";
    }
}
