using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointPatrol : MonoBehaviour
{
    public float speed;
    public List<Vector3> wayPoints;
    public List<Color> colorList;

    private Coroutine path;
    private Coroutine color;

    public Material materialColor;
    public float duration;

    // Start is called before the first frame update
    void Start()
    {
        path = StartCoroutine(PatrolWayPoints());
        color = StartCoroutine(ColorPoints());
        materialColor = gameObject.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopPatrollingAndColors();
            Debug.Log("Patroling Stoppped!");
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            path = StartCoroutine(PatrolWayPoints());
            color = StartCoroutine(ColorPoints());
            Debug.Log("Patroling Restarted!");
        }
    }

    public void StopPatrollingAndColors()
    {
        StopCoroutine(path);
        StopCoroutine(color);
    }

    protected IEnumerator ColorPoints()
    {
        float time = 0;

        while (true)
        {
            foreach (Color c in colorList)
            {
                //Debug.Log("pathing to: " + point);

                while (materialColor.color != c)
                {
                    //update position using moveTowards
                    materialColor.color = Color.Lerp(materialColor.color, c, time/duration);
                    time += Time.deltaTime;

                    yield return null;
                }

                time = 0;
            }

            yield return null;
        }
    }

    protected IEnumerator PatrolWayPoints()
    {
        while (true)
        {
            foreach (Vector3 point in wayPoints)
            {
                //Debug.Log("pathing to: " + point);

                while(transform.position != point)
                {
                    //update position using moveTowards
                    transform.position = Vector3.MoveTowards(transform.position, point, speed * Time.deltaTime);

                    yield return null; //if put WaitForSeconds here, will pause after each point
                }
            }

            yield return null; //always put just incase, so it doesn't become stuck
            //yield return new WaitForSeconds(2.0f); //stops at end of patrol
        }
    }
}
