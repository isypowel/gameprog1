using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionScript : MonoBehaviour
{
    public Vector3 initialM;
    public Vector3 endingM;
    public Vector3 initScaleM;
    public Vector3 endScaleM;
    public float endangz;
    GameObject obj;
    GameManScript gm;
    Vector3 temp;
    Vector3 temp2;

    // Start is called before the first frame update
    void Start()
    {
        obj = GameObject.FindGameObjectWithTag("GameManager");
        gm = obj.GetComponent<GameManScript>();
        temp = new Vector3(0.0f, 0.0f, 0.0f);
        temp2 = new Vector3(0.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        //movement
        temp = initialM;
        transform.position = Vector3.MoveTowards(transform.position, endingM, gm.movementSpeed * Time.deltaTime);

        if(transform.position == endingM)
        {
            initialM = endingM;
            endingM = temp;
        }

        //rotation
        Vector3 angles = transform.rotation.eulerAngles;
        float anglez = angles.z;
        if ((transform.rotation.eulerAngles.z) == endangz)
            endangz = endangz - 30;
        float angle = Mathf.LerpAngle(anglez, endangz, gm.rotationSpeed * Time.deltaTime);
        transform.eulerAngles = new Vector3(0, 0, angle);

        //scaling
        transform.localScale = Vector3.Lerp(initScaleM, endScaleM, gm.scalingSpeed * Time.time); //delta time fluctuates
        /*
        if(gm.scalingSpeed * Time.time))
        {

        }
        temp2 = initScaleM;
        initScaleM = endScaleM;
        endScaleM = temp2;
        */
    }
}