using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonScript : MonoBehaviour
{
    public float rotateSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 parentPos = transform.parent.transform.position; //getting the parent's position

        transform.RotateAround(parentPos, Vector3.forward, rotateSpeed * Time.deltaTime);
    }
}
