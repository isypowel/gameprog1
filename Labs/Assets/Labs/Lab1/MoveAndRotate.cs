using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndRotate : MonoBehaviour
{
    public float MovementSpeed;
    public float RotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float deltaSpeed1 = MovementSpeed * Time.deltaTime;
        float deltaSpeed2 = RotationSpeed * Time.deltaTime;
        string message1 = "Byeee!";
        string message2 = "Im back!";

        //moving
        transform.Translate(deltaSpeed1, deltaSpeed1, 0);
        //rotate
        transform.Rotate(new Vector3(0,0, deltaSpeed2), Space.Self);
        MonoBehaviour.print(message1);

        if(transform.position.x > 10 || transform.position.y > 10 || transform.position.x < -10 || transform.position.y < -10)
        {
            transform.position = new Vector3(0, 0, 0);
            MonoBehaviour.print(message2);
        }
    }
}
