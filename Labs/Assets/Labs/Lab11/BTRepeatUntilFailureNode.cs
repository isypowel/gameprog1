using System;
using System.Collections.Generic;

public class BTRepeatUntilFailureNode : Decorator
{
    public BTRepeatUntilFailureNode(BehaviorTree t, BTNode c) : base(t, c)
    {

    }

    public override Result Execute()
    {
        while(!(Child.Execute() == Result.Failure))
        {
            Child.Execute();
        }
        return Result.Failure;
    }
}
