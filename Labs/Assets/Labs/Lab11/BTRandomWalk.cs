﻿using System;
using UnityEngine;

public class BTRandomWalk : BTNode
{
    protected Vector3 nextDestination { get; set; }
    public float speed = 3.0f;

    public BTRandomWalk(BehaviorTree t) : base(t)
    {
        nextDestination = Vector3.zero;
        FindNextDestination();
    }

    public bool FindNextDestination()
    {
        object o;
        bool found = false;
        found = Tree.Blackboard.TryGetValue("WorldBounds", out o);

        if(found)
        {
            Rect bounds = (Rect)o;
            float x = UnityEngine.Random.value * bounds.width;
            float y = UnityEngine.Random.value * bounds.height;
            nextDestination = new Vector3(x, y, nextDestination.z);
        }

        return found;
    }

    public override Result Execute()
    {
        //if we've arrived at the point, find the next destination
        if(Tree.gameObject.transform.position == nextDestination)
        {
            if (!FindNextDestination())
                return Result.Failure;
            else
                return Result.Success;
        } else
        {
            Tree.gameObject.transform.position = Vector3.MoveTowards(Tree.gameObject.transform.position,
                                                    nextDestination, Time.deltaTime * speed);

            return Result.Running;
        }
    }
}
