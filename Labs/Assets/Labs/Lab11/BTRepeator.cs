﻿using System;
using UnityEngine;

public class BTRepeator : Decorator
{
    public BTRepeator(BehaviorTree t, BTNode c) : base(t, c)
    {
    }

    public override Result Execute()
    {
        Debug.Log("Child returned: " + Child.Execute());
        return Result.Running;
    }
}
